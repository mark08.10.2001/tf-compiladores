; ModuleID = 'Turing Machine'
source_filename = "Turing Machine"

%Node = type { [10 x i8], [8 x %Instruction] }
%Instruction = type { i8, [10 x ptr] }

@current_node = global ptr null
@inputChar = private unnamed_addr constant [6 x i8] c"carry\00", align 1
@inputChar.12 = private unnamed_addr constant [6 x i8] c"carry\00", align 1
@inputChar.21 = private unnamed_addr constant [6 x i8] c"carry\00", align 1
@inputChar.30 = private unnamed_addr constant [6 x i8] c"carry\00", align 1
@inputChar.39 = private unnamed_addr constant [6 x i8] c"carry\00", align 1
@inputChar.48 = private unnamed_addr constant [6 x i8] c"carry\00", align 1
@inputChar.57 = private unnamed_addr constant [6 x i8] c"carry\00", align 1
@inputChar.66 = private unnamed_addr constant [6 x i8] c"carry\00", align 1
@inputChar.74 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.80 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.89 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.95 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.104 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.110 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.119 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.125 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.134 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.140 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.149 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.155 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.164 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.170 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.179 = private unnamed_addr constant [5 x i8] c"done\00", align 1
@inputChar.185 = private unnamed_addr constant [5 x i8] c"done\00", align 1

define void @main() {
entry:
  %start_state = alloca [6 x i8], align 1
  %blank = alloca i8, align 1
  %tape_index = alloca i32, align 4
  %tape = alloca [7 x i8], align 1
  %nodes_array = alloca [10 x %Node], align 8
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 0
  store ptr %0, ptr @current_node, align 8
  ret void
  store [7 x i8] c" 1011 \00", ptr %tape, align 1
  store i32 0, ptr %tape_index, align 4
  store i8 32, ptr %blank, align 1
  store [6 x i8] c"right\00", ptr %start_state, align 1
  %node_ptr = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 0
}

define void @move_func() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func() {
entry:
  call void @move_func()
  ret void
}

define void @move_func.1() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.2() {
entry:
  call void @move_func.1()
  ret void
}

define void @move_func.3() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.4() {
entry:
  call void @move_func.3()
  call void @change_node_func()
  ret void
}

define void @composite_func.5() {
entry:
  call void @composite_func.4()
  ret void
}

define void @move_func.6() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.7() {
entry:
  call void @move_func.6()
  ret void
}

define void @move_func.8() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.9() {
entry:
  call void @move_func.8()
  ret void
}

define void @move_func.10() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.11() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.12, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.13() {
entry:
  call void @move_func.10()
  call void @change_node_func.11()
  ret void
}

define void @composite_func.14() {
entry:
  call void @composite_func.13()
  ret void
}

define void @move_func.15() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.16() {
entry:
  call void @move_func.15()
  ret void
}

define void @move_func.17() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.18() {
entry:
  call void @move_func.17()
  ret void
}

define void @move_func.19() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.20() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.21, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.22() {
entry:
  call void @move_func.19()
  call void @change_node_func.20()
  ret void
}

define void @composite_func.23() {
entry:
  call void @composite_func.22()
  ret void
}

define void @move_func.24() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.25() {
entry:
  call void @move_func.24()
  ret void
}

define void @move_func.26() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.27() {
entry:
  call void @move_func.26()
  ret void
}

define void @move_func.28() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.29() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.30, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.31() {
entry:
  call void @move_func.28()
  call void @change_node_func.29()
  ret void
}

define void @composite_func.32() {
entry:
  call void @composite_func.31()
  ret void
}

define void @move_func.33() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.34() {
entry:
  call void @move_func.33()
  ret void
}

define void @move_func.35() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.36() {
entry:
  call void @move_func.35()
  ret void
}

define void @move_func.37() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.38() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.39, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.40() {
entry:
  call void @move_func.37()
  call void @change_node_func.38()
  ret void
}

define void @composite_func.41() {
entry:
  call void @composite_func.40()
  ret void
}

define void @move_func.42() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.43() {
entry:
  call void @move_func.42()
  ret void
}

define void @move_func.44() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.45() {
entry:
  call void @move_func.44()
  ret void
}

define void @move_func.46() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.47() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.48, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.49() {
entry:
  call void @move_func.46()
  call void @change_node_func.47()
  ret void
}

define void @composite_func.50() {
entry:
  call void @composite_func.49()
  ret void
}

define void @move_func.51() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.52() {
entry:
  call void @move_func.51()
  ret void
}

define void @move_func.53() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.54() {
entry:
  call void @move_func.53()
  ret void
}

define void @move_func.55() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.56() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.57, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.58() {
entry:
  call void @move_func.55()
  call void @change_node_func.56()
  ret void
}

define void @composite_func.59() {
entry:
  call void @composite_func.58()
  ret void
}

define void @move_func.60() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.61() {
entry:
  call void @move_func.60()
  ret void
}

define void @move_func.62() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = add i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.63() {
entry:
  call void @move_func.62()
  ret void
}

define void @move_func.64() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.65() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.66, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.67() {
entry:
  call void @move_func.64()
  call void @change_node_func.65()
  ret void
}

define void @composite_func.68() {
entry:
  call void @composite_func.67()
  ret void
  store %Node { [10 x i8] c"right\00\00\00\00\00", [8 x %Instruction] [%Instruction { i8 32, [10 x ptr] [ptr @composite_func, ptr @composite_func.2, ptr @composite_func.5, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.7, ptr @composite_func.9, ptr @composite_func.14, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.16, ptr @composite_func.18, ptr @composite_func.23, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.25, ptr @composite_func.27, ptr @composite_func.32, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.34, ptr @composite_func.36, ptr @composite_func.41, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.43, ptr @composite_func.45, ptr @composite_func.50, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.52, ptr @composite_func.54, ptr @composite_func.59, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.61, ptr @composite_func.63, ptr @composite_func.68, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }] }, ptr %node_ptr, align 8
  store ptr %node_ptr, ptr @current_node, align 8
  %node_ptr = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 1
}

define void @write_func() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 48, ptr %1, align 1
  ret void
}

define void @move_func.69() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.70() {
entry:
  call void @write_func()
  call void @move_func.69()
  ret void
}

define void @write_func.71() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.72() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.73() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.74, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.75() {
entry:
  call void @move_func.72()
  call void @change_node_func.73()
  ret void
}

define void @composite_func.76() {
entry:
  call void @write_func.71()
  call void @composite_func.75()
  ret void
}

define void @write_func.77() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.78() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.79() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.80, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.81() {
entry:
  call void @move_func.78()
  call void @change_node_func.79()
  ret void
}

define void @composite_func.82() {
entry:
  call void @write_func.77()
  call void @composite_func.81()
  ret void
}

define void @write_func.83() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 48, ptr %1, align 1
  ret void
}

define void @move_func.84() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.85() {
entry:
  call void @write_func.83()
  call void @move_func.84()
  ret void
}

define void @write_func.86() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.87() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.88() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.89, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.90() {
entry:
  call void @move_func.87()
  call void @change_node_func.88()
  ret void
}

define void @composite_func.91() {
entry:
  call void @write_func.86()
  call void @composite_func.90()
  ret void
}

define void @write_func.92() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.93() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.94() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.95, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.96() {
entry:
  call void @move_func.93()
  call void @change_node_func.94()
  ret void
}

define void @composite_func.97() {
entry:
  call void @write_func.92()
  call void @composite_func.96()
  ret void
}

define void @write_func.98() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 48, ptr %1, align 1
  ret void
}

define void @move_func.99() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.100() {
entry:
  call void @write_func.98()
  call void @move_func.99()
  ret void
}

define void @write_func.101() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.102() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.103() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.104, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.105() {
entry:
  call void @move_func.102()
  call void @change_node_func.103()
  ret void
}

define void @composite_func.106() {
entry:
  call void @write_func.101()
  call void @composite_func.105()
  ret void
}

define void @write_func.107() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.108() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.109() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.110, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.111() {
entry:
  call void @move_func.108()
  call void @change_node_func.109()
  ret void
}

define void @composite_func.112() {
entry:
  call void @write_func.107()
  call void @composite_func.111()
  ret void
}

define void @write_func.113() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 48, ptr %1, align 1
  ret void
}

define void @move_func.114() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.115() {
entry:
  call void @write_func.113()
  call void @move_func.114()
  ret void
}

define void @write_func.116() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.117() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.118() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.119, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.120() {
entry:
  call void @move_func.117()
  call void @change_node_func.118()
  ret void
}

define void @composite_func.121() {
entry:
  call void @write_func.116()
  call void @composite_func.120()
  ret void
}

define void @write_func.122() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.123() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.124() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.125, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.126() {
entry:
  call void @move_func.123()
  call void @change_node_func.124()
  ret void
}

define void @composite_func.127() {
entry:
  call void @write_func.122()
  call void @composite_func.126()
  ret void
}

define void @write_func.128() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 48, ptr %1, align 1
  ret void
}

define void @move_func.129() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.130() {
entry:
  call void @write_func.128()
  call void @move_func.129()
  ret void
}

define void @write_func.131() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.132() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.133() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.134, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.135() {
entry:
  call void @move_func.132()
  call void @change_node_func.133()
  ret void
}

define void @composite_func.136() {
entry:
  call void @write_func.131()
  call void @composite_func.135()
  ret void
}

define void @write_func.137() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.138() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.139() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.140, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.141() {
entry:
  call void @move_func.138()
  call void @change_node_func.139()
  ret void
}

define void @composite_func.142() {
entry:
  call void @write_func.137()
  call void @composite_func.141()
  ret void
}

define void @write_func.143() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 48, ptr %1, align 1
  ret void
}

define void @move_func.144() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.145() {
entry:
  call void @write_func.143()
  call void @move_func.144()
  ret void
}

define void @write_func.146() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.147() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.148() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.149, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.150() {
entry:
  call void @move_func.147()
  call void @change_node_func.148()
  ret void
}

define void @composite_func.151() {
entry:
  call void @write_func.146()
  call void @composite_func.150()
  ret void
}

define void @write_func.152() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.153() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.154() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.155, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.156() {
entry:
  call void @move_func.153()
  call void @change_node_func.154()
  ret void
}

define void @composite_func.157() {
entry:
  call void @write_func.152()
  call void @composite_func.156()
  ret void
}

define void @write_func.158() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 48, ptr %1, align 1
  ret void
}

define void @move_func.159() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.160() {
entry:
  call void @write_func.158()
  call void @move_func.159()
  ret void
}

define void @write_func.161() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.162() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.163() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.164, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.165() {
entry:
  call void @move_func.162()
  call void @change_node_func.163()
  ret void
}

define void @composite_func.166() {
entry:
  call void @write_func.161()
  call void @composite_func.165()
  ret void
}

define void @write_func.167() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.168() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.169() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.170, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.171() {
entry:
  call void @move_func.168()
  call void @change_node_func.169()
  ret void
}

define void @composite_func.172() {
entry:
  call void @write_func.167()
  call void @composite_func.171()
  ret void
}

define void @write_func.173() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 48, ptr %1, align 1
  ret void
}

define void @move_func.174() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @composite_func.175() {
entry:
  call void @write_func.173()
  call void @move_func.174()
  ret void
}

define void @write_func.176() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.177() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.178() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.179, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.180() {
entry:
  call void @move_func.177()
  call void @change_node_func.178()
  ret void
}

define void @composite_func.181() {
entry:
  call void @write_func.176()
  call void @composite_func.180()
  ret void
}

define void @write_func.182() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = getelementptr [7 x i8], ptr %tape, i32 %0
  store i8 49, ptr %1, align 1
  ret void
}

define void @move_func.183() {
entry:
  %0 = load i32, ptr %tape_index, align 4
  %1 = sub i32 %0, 1
  store i32 %1, ptr %tape_index, align 4
  ret void
}

define void @change_node_func.184() {
entry:
  %index = alloca i32, align 4
  store i32 0, ptr %index, align 4
  br label %loop

loop:                                             ; preds = %not_found, %entry
  %curIndex = load i32, ptr %index, align 4
  %0 = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 %curIndex
  %nodeNamePtr = getelementptr inbounds %Node, ptr %0, i32 0, i32 0
  br label %checkName

checkName:                                        ; preds = %loop
  %charIndex = alloca i32, align 4
  store i32 0, ptr %charIndex, align 4
  %isEqual = alloca i1, align 1
  store i1 false, ptr %isEqual, align 1
  br label %cmpLoop

loop_end:                                         ; preds = %cmpEnd
  store ptr %0, ptr @current_node, align 8
  ret void

not_found:                                        ; preds = %not_found, %cmpEnd
  %nextIndex = add i32 %curIndex, 1
  store i32 %nextIndex, ptr %index, align 4
  %endCond = icmp eq i32 %nextIndex, 8
  br i1 %endCond, label %not_found, label %loop
  ret void

cmpLoop:                                          ; preds = %cmpLoop, %checkName
  %charIdx = load i32, ptr %charIndex, align 4
  %1 = getelementptr i8, ptr %nodeNamePtr, i32 %charIdx
  %nodeChar = load i8, ptr %1, align 1
  %2 = getelementptr i8, ptr @inputChar.185, i32 %charIdx
  %inputCharVal = load i8, ptr %2, align 1
  %charCmp = icmp eq i8 %nodeChar, %inputCharVal
  store i1 %charCmp, ptr %isEqual, align 1
  %nextCharIdx = add i32 %charIdx, 1
  store i32 %nextCharIdx, ptr %charIndex, align 4
  %cmpEndCond = icmp eq i32 %nextCharIdx, 10
  br i1 %cmpEndCond, label %cmpEnd, label %cmpLoop

cmpEnd:                                           ; preds = %cmpLoop
  %finalCmp = load i1, ptr %isEqual, align 1
  br i1 %finalCmp, label %loop_end, label %not_found
}

define void @composite_func.186() {
entry:
  call void @move_func.183()
  call void @change_node_func.184()
  ret void
}

define void @composite_func.187() {
entry:
  call void @write_func.182()
  call void @composite_func.186()
  ret void
  store %Node { [10 x i8] c"carry\00\00\00\00\00", [8 x %Instruction] [%Instruction { i8 32, [10 x ptr] [ptr @composite_func.70, ptr @composite_func.76, ptr @composite_func.82, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.85, ptr @composite_func.91, ptr @composite_func.97, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.100, ptr @composite_func.106, ptr @composite_func.112, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.115, ptr @composite_func.121, ptr @composite_func.127, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.130, ptr @composite_func.136, ptr @composite_func.142, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.145, ptr @composite_func.151, ptr @composite_func.157, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.160, ptr @composite_func.166, ptr @composite_func.172, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }, %Instruction { i8 32, [10 x ptr] [ptr @composite_func.175, ptr @composite_func.181, ptr @composite_func.187, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null, ptr null] }] }, ptr %node_ptr, align 8
  %node_ptr = getelementptr [10 x %Node], ptr %nodes_array, i32 0, i32 2
  store %Node { [10 x i8] c"done\00\00\00\00\00\00", [8 x %Instruction] zeroinitializer }, ptr %node_ptr, align 8
}
