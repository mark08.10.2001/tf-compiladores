##Integrantes
- Luciano Valentino Achin Angeles (202113625)
- Marlon Omar Llaguento de la Cruz (20201B055)
- José Giovanni Laura Silvera (202112986)

# Informe del Proyecto: Máquina de Turing utilizando LLVM

## Introducción

### Problema

Las máquinas de Turing son modelos abstractos fundamentales en la teoría de la computación, capaces de simular cualquier algoritmo. Sin embargo, la implementación práctica y la visualización de sus procesos pueden ser desafiantes debido a su naturaleza teórica.

### Motivación

La motivación de este proyecto radica en la necesidad de una herramienta que permita la implementación, ejecución y visualización de máquinas de Turing de manera eficiente y comprensible. Utilizar LLVM proporciona una plataforma robusta y flexible para desarrollar este tipo de aplicaciones.

### Solución Propuesta

Se propone la creación de un lenguaje de representación para una máquina de Turing que permita:

-   Generar un ejecutable de la máquina de Turing que al finalizar imprima el estado final de la cinta.
-   Ejecutar la máquina de Turing en tiempo de ejecución (Just-In-Time, JIT).
-   Generar la máquina de Turing como un grafo en el lenguaje DOT de Graphviz para su visualización.

## Objetivos

1.  **Definir un lenguaje de representación para la máquina de Turing.**
2.  **Implementar la generación de ejecutables utilizando LLVM.**
3.  **Permitir la ejecución JIT de la máquina de Turing.**
4.  **Generar representaciones gráficas en lenguaje DOT de Graphviz.**
5.  **Documentar el proceso y los resultados obtenidos.**

## Marco Teórico

### Máquina de Turing

Una máquina de Turing es un dispositivo teórico que consiste en una cinta infinita dividida en celdas, un cabezal que puede leer y escribir símbolos en la cinta, y un conjunto de estados finitos que gobiernan el comportamiento del sistema. Es utilizada para formalizar el concepto de computación y determinar la solvencia de problemas algorítmicos.

### LLVM

LLVM (Low-Level Virtual Machine) es una colección de herramientas de compilación que permite la optimización de código en tiempo de compilación, enlace y ejecución. Proporciona una infraestructura flexible y extensible para desarrollar compiladores y ejecutores de lenguajes de programación.

### Graphviz y DOT

Graphviz es una herramienta de software para la visualización de grafos, y DOT es el lenguaje de texto plano utilizado por Graphviz para describir estos grafos.

## Metodología

### Paso 1: Definición del Lenguaje de Representación

Se define una gramática para describir las transiciones de la máquina de Turing, los estados y los símbolos en la cinta. Esta gramática debe ser clara y permitir una fácil traducción a instrucciones de bajo nivel.

### Paso 2: Implementación en LLVM

Se utiliza LLVM para desarrollar un compilador que traduzca el lenguaje de representación de la máquina de Turing a un ejecutable. El compilador genera el código de máquina necesario para la ejecución de las transiciones y la manipulación de la cinta. Además, identifica todas las funciones y los nodos presentes en el ejemplo de máquina de turing de la página turinmachine.io. Así mismo, se implementó las optimizaciones necesarias para no hacer tener un mejor rendimiento del código y un menor peso.

### Paso 3: Ejecución JIT

Se implementa un mecanismo de ejecución JIT utilizando las capacidades de LLVM. Esto permite la compilación y ejecución dinámica de las máquinas de Turing, proporcionando una mayor flexibilidad y eficiencia.

### Paso 4: Generación de Grafos en DOT

Se desarrolla un módulo que convierte la definición de la máquina de Turing a un grafo en lenguaje DOT. Esto facilita la visualización y comprensión del funcionamiento de la máquina. Sin embargo, no se tuvo exito en el proceso y tuvo que ser descartado.

### Paso 5: Pruebas y Validación

Se realizan pruebas exhaustivas para asegurar el correcto funcionamiento del compilador, la ejecución JIT y la generación de grafos. Se utilizan ejemplos clásicos de máquinas de Turing para validar la solución.

![Imágen de compilación](imagenes\imagen_1.png)

Esta imagen muestra la lectura de cada token del lenguaje de máquina de turing. Además, se muestra cómo se captan las funciones de cada nodo y la generación de código IR.

## Conclusiones

El proyecto ha demostrado que es posible definir un lenguaje de representación para una máquina de Turing y utilizar LLVM para generar ejecutables eficientes y ejecutar código en tiempo de ejecución. La generación de grafos en DOT facilita la comprensión y visualización de las máquinas de Turing. Este enfoque proporciona una herramienta poderosa para la enseñanza y el análisis de la teoría de la computación.

Link del video: [Link](https://youtu.be/wP-K8id2MVo)
