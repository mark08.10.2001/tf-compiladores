#include "llvm/ADT/APFloat.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/ExecutionEngine/ExecutionEngine.h"
// JIT
#include "llvm/ExecutionEngine/JITSymbol.h"
#include "llvm/ExecutionEngine/Orc/CompileUtils.h"
#include "llvm/ExecutionEngine/Orc/Core.h"
#include "llvm/ExecutionEngine/Orc/ExecutionUtils.h"
#include "llvm/ExecutionEngine/Orc/IRCompileLayer.h"
#include "llvm/ExecutionEngine/Orc/JITTargetMachineBuilder.h"
#include "llvm/ExecutionEngine/Orc/RTDyldObjectLinkingLayer.h"
#include "llvm/ExecutionEngine/Orc/ThreadSafeModule.h"
#include "llvm/ExecutionEngine/SectionMemoryManager.h"
// IR
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GlobalVariable.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/PassManager.h"
#include "llvm/IR/Type.h"
#include "llvm/IR/Verifier.h"
// Optimizations
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Passes/StandardInstrumentations.h"
#include "llvm/Support/DynamicLibrary.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Target/TargetMachine.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/Transforms/Scalar/GVN.h"
#include "llvm/Transforms/Scalar/Reassociate.h"
#include "llvm/Transforms/Scalar/SimplifyCFG.h"
// C/C++
#include <cctype>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

using namespace llvm;
using namespace llvm::orc;

enum class Token : int32_t {
  eof = -1,
  input_token = -2,
  blank_token = -3,
  start_state_token = -4,
  table_token = -5,
  write_token = -6,
  direction_l = -7,
  direction_r = -8,
  colon = ':',
  lbrace = '{',
  rbrace = '}',
  lbracket = '[',
  rbracket = ']',
  comma = ',',
  newline = '\n',
  node_identifier = -9,
  expr = -10,
  number = -11,
  character = -12,
  space_char = -13,
  identifier = -14,
  instr = -15,
};

static std::string IdentifierStr;
static std::string ExprStr;
static std::string NumberStr;
static std::string LastNode = "";
static char CharacterStr;
static int gettok() {
  static int LastChar = ' ';

  // Skip any whitespace.
  while (isspace(LastChar))
    LastChar = getchar();
  if (isalpha(LastChar)) { // node_identifier: [a-zA-Z][a-zA-Z0-9]*
    IdentifierStr = LastChar;
    while (isalnum((LastChar = getchar())))
      IdentifierStr += LastChar;
    if (IdentifierStr == "EOF")
      return static_cast<int>(Token::eof);
    if (IdentifierStr == "input")
      return static_cast<int>(Token::input_token);
    if (IdentifierStr == "blank")
      return static_cast<int>(Token::blank_token);
    if (IdentifierStr == "startState")
      return static_cast<int>(Token::start_state_token);
    if (IdentifierStr == "table")
      return static_cast<int>(Token::table_token);
    if (IdentifierStr == "write")
      return static_cast<int>(Token::write_token);
    if (IdentifierStr == "L")
      return static_cast<int>(Token::direction_l);
    if (IdentifierStr == "R")
      return static_cast<int>(Token::direction_r);
    return static_cast<int>(Token::node_identifier);
  }

  if (LastChar == '\'') { // expr: '\'' [0-1# ]* '\''
    ExprStr.clear();
    ExprStr += LastChar;
    while ((LastChar = getchar()) != '\'' && LastChar != EOF)
      ExprStr += LastChar;
    if (LastChar == '\'')
      ExprStr += LastChar;
    LastChar = getchar();
    return static_cast<int>(Token::expr);
  }

  if (isdigit(LastChar)) { // number: [0-1]+
    NumberStr.clear();
    do {
      NumberStr += LastChar;
      LastChar = getchar();
    } while (isdigit(LastChar));
    return static_cast<int>(Token::number);
  }

  // Definition of a comment
  if (LastChar == '#') {
    do
      LastChar = getchar();
    while (LastChar != EOF && LastChar != '\n' && LastChar != '\r');

    if (LastChar != EOF)
      return gettok();
  }

  if (LastChar == EOF)
    return static_cast<int>(Token::eof);

  int ThisChar = LastChar;
  LastChar = getchar();
  return ThisChar;
}

//===----------------------------------------------------------------------===//
// Abstract Syntax Tree
//===----------------------------------------------------------------------===//

class ExprAST {
public:
  virtual ~ExprAST() = default;
  virtual Value *codegen() = 0;
};

class InputExprAST : public ExprAST {
  std::string InputValue;

public:
  InputExprAST(const std::string &InputValue) : InputValue(InputValue) {
  }
  Value *codegen() override;
};

class BlankExprAST : public ExprAST {
  std::string BlankValue;

public:
  BlankExprAST(const std::string &BlankValue) : BlankValue(BlankValue) {
  }
  Value *codegen() override;
};

class StartStateExprAST : public ExprAST {
  std::string StartValue;

public:
  StartStateExprAST(const std::string &StartValue) : StartValue(StartValue) {
  }
  Value *codegen() override;
};

class NodeVarExprAST : public ExprAST {
  std::string NodeName;

public:
  NodeVarExprAST(const std::string &NodeName) : NodeName(NodeName) {
  }
  std::string getName() {
    return NodeName;
  }
  Value *codegen() override;
};

class NodeExprAST : public ExprAST {
  std::string Name;
  std::vector<std::unique_ptr<ExprAST>> Cases;

public:
  NodeExprAST(const std::string &Name,
              std::vector<std::unique_ptr<ExprAST>> Cases)
      : Name(Name), Cases(std::move(Cases)) {
  }
  std::string getName() {
    return Name;
  }
  std::vector<std::unique_ptr<ExprAST>> &getCases() {
    return Cases;
  }
  Value *codegen() override;
};

// Instruction types

// Ex: { write: 0 }
class WriteExprAST : public ExprAST {
  char newVal;

public:
  WriteExprAST(char newVal) : newVal(newVal) {
  }
  char getNewVal() {
    return newVal;
  }
  Value *codegen() override;
};

// Ex: { L }
class MoveTapeExprAST : public ExprAST {
  int32_t direction;

public:
  MoveTapeExprAST(int32_t direction) : direction(direction) {
  }
  int32_t getDirection() {
    return direction;
  }
  Value *codegen() override;
};

// Ex: { L: node_name }
class MoveTapeChangeNodeExprAST : public ExprAST {
  int32_t direction;
  std::unique_ptr<NodeVarExprAST> NodeDirection;

public:
  MoveTapeChangeNodeExprAST(int32_t direction,
                            std::unique_ptr<NodeVarExprAST> NodeDirection)
      : direction(direction), NodeDirection(std::move(NodeDirection)) {
  }
  int32_t getDirection() {
    return direction;
  }
  std::unique_ptr<NodeVarExprAST> &getNodeDirection() {
    return NodeDirection;
  }
  Value *codegen() override;
};

// Ex: { node_name }
class ChangeNodeExprAST : public ExprAST {
  std::unique_ptr<NodeVarExprAST> NodeDirection;

public:
  ChangeNodeExprAST(std::unique_ptr<NodeVarExprAST> NodeDirection)
      : NodeDirection(std::move(NodeDirection)) {
  }
  std::unique_ptr<NodeVarExprAST> &getNodeDirection() {
    return NodeDirection;
  }
  Value *codegen() override;
};

class InstructionBlockAST : public ExprAST {
  std::unique_ptr<std::string> Owner;
  std::vector<std::unique_ptr<ExprAST>> Instructions;

public:
  InstructionBlockAST(std::unique_ptr<std::string> Owner,
                      std::vector<std::unique_ptr<ExprAST>> Instructions)
      : Owner(std::move(Owner)), Instructions(std::move(Instructions)) {
  }
  Value *codegen() override;
};

class CaseAST : public ExprAST {
  char Condition;
  std::unique_ptr<ExprAST> InstructionBlock;

public:
  CaseAST(char Condition, std::unique_ptr<ExprAST> InstructionBlock)
      : Condition(Condition), InstructionBlock(std::move(InstructionBlock)) {
  }
  char getCondition() {
    return Condition;
  }
  std::unique_ptr<ExprAST> &getInstructionBlock() {
    return InstructionBlock;
  }
  Value *codegen() override;
};

class BracketedCaseAST : public ExprAST {
  std::vector<char> Cases;
  std::unique_ptr<ExprAST> InstructionBlock;

public:
  BracketedCaseAST(std::vector<char> Cases,
                   std::unique_ptr<ExprAST> InstructionBlock)
      : Cases(std::move(Cases)), InstructionBlock(std::move(InstructionBlock)) {
  }
  std::vector<char> &getCases() {
    return Cases;
  }
  std::unique_ptr<ExprAST> &getInstructionBlock() {
    return InstructionBlock;
  }
  Value *codegen() override;
};

class TableExprAST : public ExprAST {
  std::vector<std::unique_ptr<ExprAST>> Nodes;

public:
  TableExprAST(std::vector<std::unique_ptr<ExprAST>> Nodes)
      : Nodes(std::move(Nodes)) {
  }
  Value *codegen() override;
};

//===----------------------------------------------------------------------===//
// Parser
//===----------------------------------------------------------------------===//

static int CurTok;
static int getNextToken() {
  return CurTok = gettok();
}

std::unique_ptr<ExprAST> LogError(const char *Str) {
  fprintf(stderr, "Error: %s\n", Str);
  return nullptr;
}

static std::unique_ptr<ExprAST> ParseInput() {
  if (CurTok != static_cast<int>(Token::input_token))
    return LogError("Expected 'input' token");
  getNextToken(); // eat input token
  if (CurTok != static_cast<int>(Token::colon))
    return LogError("Expected ':' after 'input'");
  getNextToken(); // eat colon
  if (CurTok != static_cast<int>(Token::expr))
    return LogError("Expected expression after 'input:'");
  auto Result = std::make_unique<InputExprAST>(ExprStr);
  getNextToken(); // eat expression
  return Result;
}

static std::unique_ptr<ExprAST> ParseBlank() {
  if (CurTok != static_cast<int>(Token::blank_token))
    return LogError("Expected 'blank' token");
  getNextToken(); // eat blank token
  if (CurTok != static_cast<int>(Token::colon))
    return LogError("Expected ':' after 'blank'");
  getNextToken(); // eat colon
  if (CurTok != static_cast<int>(Token::expr))
    return LogError("Expected expression after 'blank:'");
  auto Result = std::make_unique<BlankExprAST>(ExprStr);
  getNextToken(); // eat expression
  return Result;
}

static std::unique_ptr<ExprAST> ParseStartState() {
  if (CurTok != static_cast<int>(Token::start_state_token))
    return LogError("Expected 'start state' token");
  getNextToken(); // eat start state token
  if (CurTok != static_cast<int>(Token::colon))
    return LogError("Expected ':' after 'start state'");
  getNextToken(); // eat colon
  if (CurTok != static_cast<int>(Token::node_identifier))
    return LogError("Expected node identifier after 'start state:'");
  auto Result = std::make_unique<StartStateExprAST>(IdentifierStr);
  getNextToken(); // eat node identifier
  return Result;
}

static std::unique_ptr<ExprAST> ParseWrite() {
  // TODO: implement
  std::cout << "Missing implementation for write instruction" << std::endl;
  return nullptr;
}

static std::unique_ptr<ExprAST> ParseDirection() {
  // TODO: implement
  // CurTok is either direction_l or direction_r
  std::cout << "Missing implementation for direction" << std::endl;
  return nullptr;
}
/*
static std::unique_ptr<NodeVarExprAST> ParseNodeVar() {
}
*/
static std::unique_ptr<ExprAST> ParseInstruction() {
  // TODO: make sure all tokens are eaten
  std::string Case;
  if (CurTok == static_cast<int>(Token::write_token)) {
    getNextToken(); // eat write token
    if (CurTok != static_cast<int>(Token::colon))
      return LogError("Expected ':' after 'write'");
    getNextToken(); // eat colon
    if ((CurTok != static_cast<int>(Token::expr) && ExprStr.size() != 3) ||
        (CurTok != static_cast<int>(Token::number) && NumberStr.size() != 1))
      return LogError("Expected character expression after 'write:'");
    // auto Write = std::make_unique<WriteExprAST>(ExprStr[1]);
    auto Write = CurTok == static_cast<int>(Token::expr)
                     ? std::make_unique<WriteExprAST>(ExprStr[1])
                     : std::make_unique<WriteExprAST>(NumberStr[0]);
    getNextToken(); // eat character
    return Write;
  }
  if (CurTok == static_cast<int>(Token::direction_l) ||
      CurTok == static_cast<int>(Token::direction_r)) {
    int32_t direction = CurTok;
    getNextToken(); // eat direction token
    if (CurTok == static_cast<int>(Token::colon)) {
      getNextToken(); // eat colon
      if (CurTok != static_cast<int>(Token::node_identifier))
        return LogError("Expected node identifier after direction");
      auto Node = std::make_unique<NodeVarExprAST>(IdentifierStr);
      getNextToken(); // eat node identifier
      return std::make_unique<MoveTapeChangeNodeExprAST>(direction,
                                                         std::move(Node));
    }
    return std::make_unique<MoveTapeExprAST>(direction);
  }

  if (CurTok == static_cast<int>(Token::node_identifier)) {
    std::unique_ptr<NodeVarExprAST> Node =
        std::make_unique<NodeVarExprAST>(IdentifierStr);
    return std::make_unique<ChangeNodeExprAST>(std::move(Node));
  }
  return LogError("Expected instruction in ParseInstruction");
}
static std::unique_ptr<ExprAST> ParseInstructionBlock() {
  if (CurTok != static_cast<int>(Token::lbrace))
    return LogError("Expected '{' at the start of instruction block");

  getNextToken(); // eat '{'

  std::vector<std::unique_ptr<ExprAST>> Instructions;

  while (CurTok != static_cast<int>(Token::rbrace)) {
    auto Instruction = ParseInstruction();
    if (!Instruction)
      return LogError("Expected instruction in instruction block");
    Instructions.push_back(std::move(Instruction));

    if (CurTok == static_cast<int>(Token::comma))
      getNextToken(); // eat comma
  }
  getNextToken(); // eat '}'
  return std::make_unique<InstructionBlockAST>(
      std::make_unique<std::string>(LastNode), std::move(Instructions));
}
static std::unique_ptr<ExprAST> ParseCase() {
  char condition;
  if (CurTok == static_cast<int>(Token::expr) && ExprStr.size() == 3)
    condition = ExprStr[1];
  else if (CurTok == static_cast<int>(Token::number)) {
    if (NumberStr.size() != 1)
      return LogError("Expected single character after 'case'");
    condition = NumberStr[0];
  } else
    return LogError("Expected formatted condition in case");
  getNextToken(); // eat condition
  if (CurTok != static_cast<int>(Token::colon))
    return LogError("Expected ':' after condition in case");
  getNextToken(); // eat colon
  auto Instructions = ParseInstructionBlock();
  if (!Instructions)
    return LogError("Expected instruction block in case");
  return std::make_unique<CaseAST>(condition, std::move(Instructions));
}
static std::unique_ptr<ExprAST> ParseBracketedCase() {
  getNextToken(); // eat '['
  std::vector<char> conditions;
  while (CurTok != static_cast<int>(Token::rbracket)) {
    if (CurTok == static_cast<int>(Token::expr) && ExprStr.size() == 3)
      conditions.push_back(ExprStr[1]);
    else if (CurTok == static_cast<int>(Token::number)) {
      if (NumberStr.size() != 1)
        return LogError("Expected single character after 'case'");
      conditions.push_back(NumberStr[0]);
    } else
      return LogError("Expected formatted condition in bracketed case");
    getNextToken(); // eat condition
    if (CurTok == static_cast<int>(Token::comma))
      getNextToken(); // eat comma
  }
  getNextToken(); // eat ']'
  if (CurTok != static_cast<int>(Token::colon))
    return LogError("Expected ':' after condition in bracketed case");
  getNextToken(); // eat colon
  auto Instructions = ParseInstructionBlock();
  if (!Instructions)
    return nullptr;
  return std::make_unique<BracketedCaseAST>(std::move(conditions),
                                            std::move(Instructions));
}
static std::unique_ptr<ExprAST> ParseNode() {
  if (CurTok != static_cast<int>(Token::node_identifier))
    return LogError("Expected node identifier");
  std::string NodeName = IdentifierStr;
  LastNode = NodeName;
  getNextToken(); // eat node identifier

  if (CurTok != static_cast<int>(Token::lbrace))
    return LogError("Expected '{' after node identifier in node");
  getNextToken(); // eat '{'

  std::vector<std::unique_ptr<ExprAST>> Cases;
  while (CurTok != static_cast<int>(Token::rbrace)) {
    if (CurTok == static_cast<int>(Token::lbracket)) {
      auto Instr = ParseBracketedCase();
      if (!Instr)
        return LogError("Expected bracketed case in node");
      Cases.push_back(std::move(Instr));
    } else if ((CurTok == static_cast<int>(Token::expr) &&
                ExprStr.size() == 3) ||
               CurTok == static_cast<int>(Token::number)) {

      auto Instr = ParseCase();
      if (!Instr)
        return LogError("Expected case in node");
      Cases.push_back(std::move(Instr));
    } else
      return LogError("Expected case or bracketed case in node");
  }

  getNextToken(); // eat '}'
  return std::make_unique<NodeExprAST>(NodeName, std::move(Cases));
}
static std::unique_ptr<ExprAST> ParseTable() {
  if (CurTok != static_cast<int>(Token::table_token))
    return LogError("Expected 'table' token");
  getNextToken(); // eat table token
  if (CurTok != static_cast<int>(Token::lbrace))
    return LogError("Expected '{' at the start of table");
  getNextToken(); // eat '{'

  std::vector<std::unique_ptr<ExprAST>> Nodes;
  while (CurTok != static_cast<int>(Token::rbrace)) {

    std::unique_ptr<ExprAST> Node = ParseNode();
    if (!Node)
      return LogError("Expected node in table");
    Nodes.push_back(std::move(Node));
  }

  getNextToken(); // eat '}'
  return std::make_unique<TableExprAST>(std::move(Nodes));
}
//===----------------------------------------------------------------------===//
// Code Generation
//===----------------------------------------------------------------------===//

// Initialize the global LLVM context, module, and IR builder
static std::unique_ptr<LLVMContext> TheContext;
static std::unique_ptr<Module> TheModule;
static std::unique_ptr<IRBuilder<>> Builder;
static std::map<std::string, AllocaInst *> NamedValues;
static ExitOnError ExitOnErr;

static Type *Int32Type = nullptr;
static Function *MainFunction = nullptr;

// Types for nodes
static Type *UInt8Type = nullptr;
static StructType *NodeType = nullptr;
static ArrayType *NodeArrayType = nullptr;

static std::string StartState;

Function *createWriteFunction(char newVal);
Function *createMoveFunction(bool isLeft);
Function *createChangeNodeFunction(const std::string &nodeName);
Function *createCompositeFunction(std::vector<Value *> &functions);

// Optimizer
static std::unique_ptr<FunctionPassManager> TheFPM;
static std::unique_ptr<LoopAnalysisManager> TheLAM;
static std::unique_ptr<FunctionAnalysisManager> TheFAM;
static std::unique_ptr<CGSCCAnalysisManager> TheCGAM;
static std::unique_ptr<ModuleAnalysisManager> TheMAM;
static std::unique_ptr<PassInstrumentationCallbacks> ThePIC;
static std::unique_ptr<StandardInstrumentations> TheSI;

Value *findNodeIndexByName(const std::string &nodeName,
                           const std::vector<std::unique_ptr<ExprAST>> &Nodes) {
  for (size_t i = 0; i < Nodes.size(); ++i) {
    if (NodeExprAST *node = dynamic_cast<NodeExprAST *>(Nodes[i].get())) {
      if (node->getName() == nodeName) {
        return ConstantInt::get(Int32Type, i);
      }
    }
  }
  return ConstantInt::get(Int32Type, -1); // Return -1 if not found
}

// Utility function to initialize the node type

// Node {
//   string node_name,
//   vector<pair<char, vector<Function>>> InstructionFields,
// }
// Example: {{' ', }}
void initializeNodeArrayType() {
  std::vector<Type *> InstructionFields;
  InstructionFields.push_back(Type::getInt8Ty(*TheContext)); // char
  InstructionFields.push_back(ArrayType::get(
      PointerType::get(FunctionType::get(Type::getVoidTy(*TheContext), false)
                           ->getPointerTo(),
                       0),
      10)); // vector<Function>
  StructType *InstructionType =
      StructType::create(*TheContext, InstructionFields, "Instruction");

  // Define the Node type
  std::vector<Type *> NodeFields;
  // node_name
  NodeFields.push_back(ArrayType::get(Type::getInt8Ty(*TheContext),
                                      10)); // node_name (array of chars)
  NodeFields.push_back(
      ArrayType::get(InstructionType, 8)); // vector of Instructions

  NodeType = StructType::create(*TheContext, NodeFields, "Node");
  // Modify node array length
  NodeArrayType = ArrayType::get(NodeType, 10);
}
void initializeGlobals() {
  TheContext = std::make_unique<LLVMContext>();
  TheModule = std::make_unique<Module>("Turing Machine", *TheContext);
  Builder = std::make_unique<IRBuilder<>>(*TheContext);

  Int32Type = Type::getInt32Ty(*TheContext);
  UInt8Type = Type::getInt8Ty(*TheContext);

  Constant *Zero = ConstantInt::get(Int32Type, 0);

  //===----------------------------------------------------------------------===//
  // Optimizer
  //===----------------------------------------------------------------------===//
  // Optimization
  // Passes
  TheFPM = std::make_unique<FunctionPassManager>();
  TheLAM = std::make_unique<LoopAnalysisManager>();
  TheFAM = std::make_unique<FunctionAnalysisManager>();
  TheCGAM = std::make_unique<CGSCCAnalysisManager>();
  TheMAM = std::make_unique<ModuleAnalysisManager>();
  ThePIC = std::make_unique<PassInstrumentationCallbacks>();
  // TheSI = std::make_unique<StandardInstrumentations>(*TheContext, true);
  // TheSI->registerCallbacks(*ThePIC, *TheMAM);

  PassBuilder PB;
  PB.registerModuleAnalyses(*TheMAM);
  PB.registerCGSCCAnalyses(*TheCGAM);
  PB.registerFunctionAnalyses(*TheFAM);
  PB.registerLoopAnalyses(*TheLAM);
  PB.crossRegisterProxies(*TheLAM, *TheFAM, *TheCGAM, *TheMAM);

  FunctionPassManager FPM;
  FPM.addPass(InstCombinePass());
  FPM.addPass(ReassociatePass());
  FPM.addPass(GVNPass());
  FPM.addPass(SimplifyCFGPass());

  *TheFPM = std::move(FPM);

  initializeNodeArrayType();
  GlobalVariable *currentNode = new GlobalVariable(
      *TheModule, PointerType::getUnqual(NodeType), false,
      GlobalValue::ExternalLinkage,
      ConstantPointerNull::get(PointerType::getUnqual(NodeType)),
      "current_node");
}

static AllocaInst *CreateEntryBlockAlloca(Function *TheFunction, Type *VarType,
                                          const std::string &VarName,
                                          Value *ArraySize = nullptr) {
  IRBuilder<> TmpB(&TheFunction->getEntryBlock(),
                   TheFunction->getEntryBlock().begin());
  return TmpB.CreateAlloca(VarType, ArraySize, VarName);
}

// Main function will encapsulate all generated code
// so that we can easily create memory allocated variables
Function *createMainFunction() {
  FunctionType *FuncType =
      FunctionType::get(Type::getVoidTy(*TheContext), false);
  MainFunction =
      Function::Create(FuncType, Function::ExternalLinkage, "main", *TheModule);
  BasicBlock *EntryBB = BasicBlock::Create(*TheContext, "entry", MainFunction);
  Builder->SetInsertPoint(EntryBB);

  // Allocate nodes array in main
  AllocaInst *NodesArrayAlloca =
      Builder->CreateAlloca(NodeArrayType, nullptr, "nodes_array");
  NamedValues["nodes_array"] = NodesArrayAlloca;
  GlobalVariable *currentNode = TheModule->getNamedGlobal("current_node");
  Value *FirstNodePtr =
      Builder->CreateGEP(NodeArrayType, NodesArrayAlloca,
                         {Builder->getInt32(0), Builder->getInt32(0)});
  Builder->CreateStore(FirstNodePtr, currentNode);
  Builder->CreateRetVoid();
  return MainFunction;
}

Value *LogErrorV(const char *Str) {
  LogError(Str);
  return nullptr;
}

bool isMainFunction() {
  return Builder->GetInsertBlock()->getParent() == MainFunction;
}

Value *InputExprAST::codegen() {
  createMainFunction();
  Function *MainFunc = Builder->GetInsertBlock()->getParent();
  if (MainFunc != MainFunction) {
    return LogErrorV("Current Block is not main");
  }
  // "'input'" => " input "
  // TODO: check if this works
  std::string val = InputValue;
  val[0] = ' ';
  val[val.size() - 1] = ' ';

  ArrayType *ArrayTy =
      ArrayType::get(IntegerType::get(*TheContext, 8), val.size() + 1);
  AllocaInst *TapeAlloca =
      CreateEntryBlockAlloca(MainFunction, ArrayTy, "tape");
  Constant *InitialData = ConstantDataArray::getString(*TheContext, val, true);
  Builder->CreateStore(InitialData, TapeAlloca);

  // create tape_index
  AllocaInst *TapeIndexAlloca =
      CreateEntryBlockAlloca(MainFunction, Int32Type, "tape_index");
  Builder->CreateStore(ConstantInt::get(Int32Type, 0), TapeIndexAlloca);

  NamedValues["tape"] = TapeAlloca;
  NamedValues["tape_index"] = TapeIndexAlloca;
  return TapeAlloca;
}

Value *BlankExprAST::codegen() {
  Function *MainFunc = Builder->GetInsertBlock()->getParent();
  if (MainFunc != MainFunction) {
    return LogErrorV("Current Block is not main");
  }
  // Assuming format ' '
  char blankVal = BlankValue[1];
  Type *CharType = Type::getInt8Ty(*TheContext);
  AllocaInst *BlankAlloca =
      CreateEntryBlockAlloca(MainFunction, CharType, "blank");
  Constant *BlankData = ConstantInt::get(CharType, blankVal);
  Builder->CreateStore(BlankData, BlankAlloca);

  return BlankAlloca;
}

Value *StartStateExprAST::codegen() {
  Function *MainFunc = Builder->GetInsertBlock()->getParent();
  if (MainFunc != MainFunction) {
    return LogErrorV("Current Block is not main");
  }
  // TODO: assert StartValue.size() > 2
  std::string startVal = StartValue;
  ArrayType *ArrayTy =
      ArrayType::get(IntegerType::get(*TheContext, 8), StartValue.size() + 1);
  AllocaInst *StartStateAlloca =
      CreateEntryBlockAlloca(MainFunction, ArrayTy, "start_state");
  Constant *StartData =
      ConstantDataArray::getString(*TheContext, startVal, true);
  Builder->CreateStore(StartData, StartStateAlloca);
  StartState = StartValue;
  return StartStateAlloca;
}

Value *NodeVarExprAST::codegen() {

  return nullptr;
}

Value *NodeExprAST::codegen() {
  for (auto &Case : Cases) {
    Case->codegen();
  }
  return nullptr;
}

Value *InstructionBlockAST::codegen() {
  std::vector<Value *> functions;

  for (auto &instr : Instructions) {
    std::cout << "Processing instruction..." << std::endl;
    if (auto *writeInstr = dynamic_cast<WriteExprAST *>(instr.get())) {
      std::cout << "Write instruction" << std::endl;
      Value *writeFunc = writeInstr->codegen();
      if (!writeFunc) {
        return LogErrorV("Failed to generate write function");
      }
      functions.push_back(writeFunc);
    } else if (auto *moveInstr = dynamic_cast<MoveTapeExprAST *>(instr.get())) {
      std::cout << "Move instruction" << std::endl;
      Value *moveFunc = moveInstr->codegen();
      if (!moveFunc) {
        return LogErrorV("Failed to generate move function");
      }
      functions.push_back(moveFunc);
    } else if (auto *moveChangeInstr =
                   dynamic_cast<MoveTapeChangeNodeExprAST *>(instr.get())) {
      std::cout << "Move and change node instruction" << std::endl;
      Value *moveChangeFunc = moveChangeInstr->codegen();
      std::cout << "codegen for change node instr" << std::endl;
      if (!moveChangeFunc) {
        return LogErrorV("Failed to generate move and change node function");
      }
      functions.push_back(moveChangeFunc);
    } else if (auto *changeNodeInstr =
                   dynamic_cast<ChangeNodeExprAST *>(instr.get())) {
      std::cout << "Change node instruction" << std::endl;
      Value *changeNodeFunc = changeNodeInstr->codegen();
      if (!changeNodeFunc) {
        return LogErrorV("Failed to generate change node function");
      }
      functions.push_back(changeNodeFunc);
    } else {
      return LogErrorV("Unknown instruction in block");
    }
  }

  return createCompositeFunction(functions);
}

Value *CaseAST::codegen() {
  return InstructionBlock->codegen();
}

Value *BracketedCaseAST::codegen() {
  return InstructionBlock->codegen();
}

Value *WriteExprAST::codegen() {
  return createWriteFunction(newVal);
}

Value *MoveTapeExprAST::codegen() {
  return createMoveFunction(direction == static_cast<int>(Token::direction_l));
}

Value *MoveTapeChangeNodeExprAST::codegen() {
  std::cout << "Generating move and change node function for direction: "
            << (direction == static_cast<int>(Token::direction_l) ? "Left"
                                                                  : "Right")
            << " and node: " << NodeDirection->getName() << std::endl;

  std::vector<Value *> funcs;

  Value *moveFunc =
      createMoveFunction(direction == static_cast<int>(Token::direction_l));
  if (!moveFunc) {
    std::cerr << "Error generating move function." << std::endl;
    return nullptr;
  }
  std::cout << "moveFunc: " << moveFunc << std::endl;
  funcs.push_back(moveFunc);
  std::cout << "pushed back: " << moveFunc << std::endl;
  Value *changeNodeFunc = createChangeNodeFunction(NodeDirection->getName());
  if (!changeNodeFunc) {
    std::cerr << "Error generating change node function." << std::endl;
    return nullptr;
  }
  std::cout << "changeNodeFunc: " << changeNodeFunc << std::endl;
  funcs.push_back(changeNodeFunc);

  Value *compositeFunc = createCompositeFunction(funcs);
  if (!compositeFunc) {
    std::cerr << "Error creating composite function." << std::endl;
    return nullptr;
  }

  std::cout << "Successfully generated move and change node function."
            << std::endl;
  return compositeFunc;
}

Value *ChangeNodeExprAST::codegen() {
  return createChangeNodeFunction(NodeDirection->getName());
}

Value *TableExprAST::codegen() {
  Function *MainFunc = Builder->GetInsertBlock()->getParent();

  AllocaInst *NodesArrayAlloca = nullptr;

  // Find the nodes array in main
  for (auto &BB : *MainFunc) {
    for (auto &Inst : BB) {
      if (auto *Alloca = dyn_cast<AllocaInst>(&Inst)) {
        if (Alloca->getName() == "nodes_array") {
          NodesArrayAlloca = Alloca;
          break;
        }
      }
    }
    if (NodesArrayAlloca)
      break;
  }

  if (!NodesArrayAlloca) {
    return LogErrorV("Nodes array not found in main function");
  }

  StructType *InstructionType =
      dyn_cast<StructType>(NodeType->getElementType(1)->getArrayElementType());

  GlobalVariable *currentNode = TheModule->getNamedGlobal("current_node");

  for (size_t i = 0; i < Nodes.size(); ++i) {

    std::vector<Value *> Indices;
    Indices.push_back(ConstantInt::get(Int32Type, 0)); // base index
    Indices.push_back(ConstantInt::get(Int32Type, i)); // element index
    Value *NodePtr = Builder->CreateGEP(NodeArrayType, NodesArrayAlloca,
                                        Indices, "node_ptr");

    NodeExprAST *Node = static_cast<NodeExprAST *>(Nodes[i].get());
    std::vector<Constant *> NodeValues;

    // Convert node name to a constant array of characters
    std::string nodeName = Node->getName();
    std::vector<Constant *> nameChars;
    for (char c : nodeName) {
      nameChars.push_back(ConstantInt::get(Type::getInt8Ty(*TheContext), c));
    }
    nameChars.resize(10, ConstantInt::get(Type::getInt8Ty(*TheContext),
                                          0)); // Ensure it has 10 chars
    NodeValues.push_back(ConstantArray::get(
        ArrayType::get(Type::getInt8Ty(*TheContext), 10), nameChars));

    // Initialize instructions array with pairs of char and empty vectors of
    // function pointers
    std::vector<Constant *> Instructions;

    for (int j = 0; j < 8; ++j) {
      std::vector<Constant *> InstructionValues;
      InstructionValues.push_back(ConstantInt::get(Type::getInt8Ty(*TheContext),
                                                   0)); // Initialize char to 0

      std::vector<Constant *> FunctionPtrs;
      for (auto &caseAST : Node->getCases()) {
        if (CaseAST *caseASTPtr = dynamic_cast<CaseAST *>(caseAST.get())) {
          InstructionValues[0] = ConstantInt::get(Type::getInt8Ty(*TheContext),
                                                  caseASTPtr->getCondition());
          Value *instrValue = caseASTPtr->getInstructionBlock()->codegen();
          if (!instrValue) {
            errs() << "Error: caseASTPtr->getInstructionBlock()->codegen() "
                      "returned nullptr\n";
            continue;
          }
          if (Function *instrFunc = dyn_cast<Function>(instrValue)) {
            FunctionPtrs.push_back(instrFunc);
          } else {
            errs() << "Error: Failed to cast instrValue to Function\n";
          }
        } else if (BracketedCaseAST *bracketedCaseASTPtr =
                       dynamic_cast<BracketedCaseAST *>(caseAST.get())) {
          for (char condition : bracketedCaseASTPtr->getCases()) {
            InstructionValues[0] =
                ConstantInt::get(Type::getInt8Ty(*TheContext), condition);
            Value *instrValue =
                bracketedCaseASTPtr->getInstructionBlock()->codegen();
            if (!instrValue) {
              errs() << "Error: "
                        "bracketedCaseASTPtr->getInstructionBlock()->codegen() "
                        "returned nullptr\n";
              continue;
            }
            if (Function *instrFunc = dyn_cast<Function>(instrValue)) {
              FunctionPtrs.push_back(instrFunc);
            } else {
              errs() << "Error: Failed to cast instrValue to Function\n";
            }
          }
        }
      }

      // If there are not enough functions, pad with null pointers
      while (FunctionPtrs.size() < 10) {
        FunctionPtrs.push_back(ConstantPointerNull::get(
            Type::getVoidTy(*TheContext)->getPointerTo()));
      }

      auto FuncArrayType =
          ArrayType::get(Type::getVoidTy(*TheContext)->getPointerTo(), 10);
      auto FuncArray = ConstantArray::get(FuncArrayType, FunctionPtrs);

      InstructionValues.push_back(FuncArray);

      auto InstrStruct =
          ConstantStruct::get(InstructionType, InstructionValues);
      if (!InstrStruct) {
        errs() << "Error: Failed to create InstructionStruct\n";
      } else {
        Instructions.push_back(InstrStruct);
      }
    }
    NodeValues.push_back(
        ConstantArray::get(ArrayType::get(InstructionType, 8), Instructions));

    Constant *NodeStruct = ConstantStruct::get(NodeType, NodeValues);
    Builder->CreateStore(NodeStruct, NodePtr);

    // Check if nodeName matches start state and initialize current_node
    if (nodeName == StartState) {
      std::cout << "Start state found" << std::endl;
      Builder->CreateStore(NodePtr, currentNode);
    }
  }

  return Constant::getNullValue(Type::getVoidTy(*TheContext));
}

Function *createWriteFunction(char newVal) {
  FunctionType *funcType =
      FunctionType::get(Type::getVoidTy(*TheContext), false);
  Function *writeFunc = Function::Create(funcType, Function::ExternalLinkage,
                                         "write_func", *TheModule);
  BasicBlock *entry = BasicBlock::Create(*TheContext, "entry", writeFunc);
  Builder->SetInsertPoint(entry);

  AllocaInst *tape = NamedValues["tape"];
  AllocaInst *tapeIndex = NamedValues["tape_index"];

  Value *index = Builder->CreateLoad(Int32Type, tapeIndex);
  Value *tapePtr = Builder->CreateGEP(tape->getAllocatedType(), tape, index);
  Builder->CreateStore(ConstantInt::get(Type::getInt8Ty(*TheContext), newVal),
                       tapePtr);
  Builder->CreateRetVoid();

  return writeFunc;
}

Function *createMoveFunction(bool isLeft) {
  FunctionType *funcType =
      FunctionType::get(Type::getVoidTy(*TheContext), false);
  Function *moveFunc = Function::Create(funcType, Function::ExternalLinkage,
                                        "move_func", *TheModule);
  BasicBlock *entry = BasicBlock::Create(*TheContext, "entry", moveFunc);
  Builder->SetInsertPoint(entry);

  std::cout << "Creating move function, isLeft: " << isLeft << std::endl;

  // Check if tape_index is available in NamedValues
  if (NamedValues.find("tape_index") == NamedValues.end()) {
    std::cout << "Error: tape_index not found in NamedValues" << std::endl;
    return nullptr;
  }

  AllocaInst *tapeIndex = NamedValues["tape_index"];
  if (!tapeIndex) {
    std::cout << "Error: tape_index is null" << std::endl;
    return nullptr;
  }

  std::cout << "tape_index found, proceeding with code generation" << std::endl;

  Value *index = Builder->CreateLoad(Int32Type, tapeIndex);
  std::cout << "Loaded tape_index value" << std::endl;

  Value *newIndex =
      isLeft ? Builder->CreateSub(index, ConstantInt::get(Int32Type, 1))
             : Builder->CreateAdd(index, ConstantInt::get(Int32Type, 1));
  std::cout << "Calculated new index" << std::endl;

  Builder->CreateStore(newIndex, tapeIndex);
  std::cout << "Stored new index back to tape_index" << std::endl;

  Builder->CreateRetVoid();
  std::cout << "Created move function successfully" << std::endl;

  return moveFunc;
}
Function *createChangeNodeFunction(const std::string &nodeName) {
  std::cout << "Creating change node function for node: " << nodeName
            << std::endl;

  FunctionType *funcType =
      FunctionType::get(Type::getVoidTy(*TheContext), false);
  Function *changeNodeFunc = Function::Create(
      funcType, Function::ExternalLinkage, "change_node_func", *TheModule);
  BasicBlock *entry = BasicBlock::Create(*TheContext, "entry", changeNodeFunc);
  BasicBlock *loop = BasicBlock::Create(*TheContext, "loop", changeNodeFunc);
  BasicBlock *checkName =
      BasicBlock::Create(*TheContext, "checkName", changeNodeFunc);
  BasicBlock *loopEnd =
      BasicBlock::Create(*TheContext, "loop_end", changeNodeFunc);
  BasicBlock *notFound =
      BasicBlock::Create(*TheContext, "not_found", changeNodeFunc);

  Builder->SetInsertPoint(entry);

  GlobalVariable *currentNode = TheModule->getNamedGlobal("current_node");
  if (!currentNode) {
    std::cerr << "Error: current_node is null" << std::endl;
    return nullptr;
  }
  std::cout << "currentNode: " << currentNode << std::endl;

  AllocaInst *nodesArray = NamedValues["nodes_array"];
  if (!nodesArray) {
    std::cerr << "Error: nodesArray is null" << std::endl;
    return nullptr;
  }
  std::cout << "nodesArray: " << nodesArray << std::endl;

  AllocaInst *index = Builder->CreateAlloca(Int32Type, nullptr, "index");
  Builder->CreateStore(ConstantInt::get(Int32Type, 0), index);

  Builder->CreateBr(loop);

  Builder->SetInsertPoint(loop);

  Value *curIndex = Builder->CreateLoad(Int32Type, index, "curIndex");
  Value *nodePtr = Builder->CreateGEP(
      NodeArrayType, nodesArray, {ConstantInt::get(Int32Type, 0), curIndex});
  std::cout << "nodePtr: " << nodePtr << std::endl;

  // Access the node name directly using StructGEP
  Value *nodeNamePtr =
      Builder->CreateStructGEP(NodeType, nodePtr, 0, "nodeNamePtr");
  if (!nodeNamePtr) {
    std::cerr << "Error: nodeNamePtr is null" << std::endl;
    return nullptr;
  }
  std::cout << "nodeNamePtr: " << nodeNamePtr << std::endl;

  Builder->CreateBr(checkName);

  Builder->SetInsertPoint(checkName);

  // Create loop to compare nodeName and nodeNamePtr
  BasicBlock *cmpLoop =
      BasicBlock::Create(*TheContext, "cmpLoop", changeNodeFunc);
  BasicBlock *cmpEnd =
      BasicBlock::Create(*TheContext, "cmpEnd", changeNodeFunc);

  AllocaInst *charIndex =
      Builder->CreateAlloca(Int32Type, nullptr, "charIndex");
  Builder->CreateStore(ConstantInt::get(Int32Type, 0), charIndex);
  AllocaInst *isEqual =
      Builder->CreateAlloca(Type::getInt1Ty(*TheContext), nullptr, "isEqual");
  Builder->CreateStore(ConstantInt::getFalse(*TheContext), isEqual);

  Builder->CreateBr(cmpLoop);

  Builder->SetInsertPoint(cmpLoop);

  Value *charIdx = Builder->CreateLoad(Int32Type, charIndex, "charIdx");
  Value *nodeCharPtr =
      Builder->CreateGEP(Type::getInt8Ty(*TheContext), nodeNamePtr, charIdx);
  if (!nodeCharPtr) {
    std::cerr << "Error: nodeCharPtr is null" << std::endl;
    return nullptr;
  }
  std::cout << "nodeCharPtr: " << nodeCharPtr << std::endl;

  Value *nodeChar = Builder->CreateLoad(Type::getInt8Ty(*TheContext),
                                        nodeCharPtr, "nodeChar");
  if (!nodeChar) {
    std::cerr << "Error: nodeChar is null" << std::endl;
    return nullptr;
  }
  std::cout << "nodeChar: " << nodeChar << std::endl;

  Value *inputChar = Builder->CreateGlobalStringPtr(nodeName, "inputChar");
  Value *inputCharElem =
      Builder->CreateGEP(Type::getInt8Ty(*TheContext), inputChar, charIdx);
  Value *inputCharVal = Builder->CreateLoad(Type::getInt8Ty(*TheContext),
                                            inputCharElem, "inputCharVal");

  Value *charCmp = Builder->CreateICmpEQ(nodeChar, inputCharVal, "charCmp");
  Builder->CreateStore(charCmp, isEqual);

  Value *nextCharIdx = Builder->CreateAdd(
      charIdx, ConstantInt::get(Int32Type, 1), "nextCharIdx");
  Builder->CreateStore(nextCharIdx, charIndex);

  Value *cmpEndCond = Builder->CreateICmpEQ(
      nextCharIdx, ConstantInt::get(Int32Type, 10), "cmpEndCond");
  Builder->CreateCondBr(cmpEndCond, cmpEnd, cmpLoop);

  Builder->SetInsertPoint(cmpEnd);

  Value *finalCmp =
      Builder->CreateLoad(Type::getInt1Ty(*TheContext), isEqual, "finalCmp");
  Builder->CreateCondBr(finalCmp, loopEnd, notFound);

  Builder->SetInsertPoint(notFound);

  // Increment the index
  Value *nextIndex =
      Builder->CreateAdd(curIndex, ConstantInt::get(Int32Type, 1), "nextIndex");
  Builder->CreateStore(nextIndex, index);

  // Check if we've reached the end of the array
  Value *endCond = Builder->CreateICmpEQ(
      nextIndex, ConstantInt::get(Int32Type, 8), "endCond");
  Builder->CreateCondBr(endCond, notFound, loop);

  Builder->SetInsertPoint(loopEnd);

  // Store the found node pointer
  Builder->CreateStore(nodePtr, currentNode);
  Builder->CreateRetVoid();

  Builder->SetInsertPoint(notFound);
  // Handle node not found case
  // You can either create an error or just return void
  Builder->CreateRetVoid();

  std::cout << "Created change node function successfully" << std::endl;
  return changeNodeFunc;
}

Function *createCompositeFunction(std::vector<Value *> &functions) {
  FunctionType *funcType =
      FunctionType::get(Type::getVoidTy(*TheContext), false);
  Function *compositeFunc = Function::Create(
      funcType, Function::ExternalLinkage, "composite_func", *TheModule);
  BasicBlock *entry = BasicBlock::Create(*TheContext, "entry", compositeFunc);
  Builder->SetInsertPoint(entry);

  for (auto &func : functions) {
    if (Function *funcCallee = dyn_cast<Function>(func)) {
      Builder->CreateCall(funcCallee, {});
    } else {
      std::cout << "Failed to cast Value to Function in composite function"
                << std::endl;
      return nullptr;
    }
  }

  Builder->CreateRetVoid();
  return compositeFunc;
}

//===----------------------------------------------------------------------===//
// Top Level parsing and JIT Driver
//===----------------------------------------------------------------------===//

class TuringJIT {
private:
  std::unique_ptr<ExecutionSession> ES;
  DataLayout DL;
  MangleAndInterner Mangle;
  RTDyldObjectLinkingLayer ObjectLayer;
  IRCompileLayer CompileLayer;
  ThreadSafeContext Ctx;
  JITDylib &MainJD;

public:
  TuringJIT(std::unique_ptr<ExecutionSession> ES, JITTargetMachineBuilder JTMB,
            DataLayout DL)
      : ES(std::move(ES)), DL(std::move(DL)), Mangle(*this->ES, this->DL),
        Ctx(std::make_unique<LLVMContext>()),
        ObjectLayer(*this->ES,
                    []() { return std::make_unique<SectionMemoryManager>(); }),
        CompileLayer(*this->ES, ObjectLayer,
                     std::make_unique<ConcurrentIRCompiler>(std::move(JTMB))),
        MainJD(this->ES->createBareJITDylib("<main>")) {
    MainJD.addGenerator(
        cantFail(DynamicLibrarySearchGenerator::GetForCurrentProcess(
            DL.getGlobalPrefix())));
    if (JTMB.getTargetTriple().isOSBinFormatCOFF()) {
      ObjectLayer.setOverrideObjectFlagsWithResponsibilityFlags(true);
      ObjectLayer.setAutoClaimResponsibilityForObjectSymbols(true);
    }
  }

  ~TuringJIT() {
    if (auto Err = ES->endSession())
      ES->reportError(std::move(Err));
  }

  static Expected<std::unique_ptr<TuringJIT>> Create() {
    auto EPC = SelfExecutorProcessControl::Create();
    if (!EPC)
      return EPC.takeError();

    auto ES = std::make_unique<ExecutionSession>(std::move(*EPC));
    auto JTMB = JITTargetMachineBuilder::detectHost();
    if (!JTMB)
      return JTMB.takeError();
    auto DL = JTMB->getDefaultDataLayoutForTarget();
    if (!DL)
      return DL.takeError();
    return std::make_unique<TuringJIT>(std::move(ES), std::move(*JTMB),
                                       std::move(*DL));
  }

  const DataLayout &getDataLayout() const {
    return DL;
  }

  JITDylib &getMainJITDylib() {
    return MainJD;
  }

  void addModule(std::unique_ptr<Module> M) {
    cantFail(CompileLayer.add(MainJD, ThreadSafeModule(std::move(M), Ctx)));
  }
};

void handleParseInput() {
  auto Input = ParseInput();
  if (!Input) {
    exit(1);
  }
  Input->codegen();
}
void handleParseBlank() {
  auto Blank = ParseBlank();
  if (!Blank) {
    exit(1);
  }
  Blank->codegen();
}

void handleParseStartState() {
  auto StartState = ParseStartState();
  if (!StartState) {
    exit(1);
  }
  StartState->codegen();
}

void handleParseTable() {
  auto Table = ParseTable();
  if (!Table) {
    exit(1);
  }
  Table->codegen();
}

//===----------------------------------------------------------------------===//
// Main driver code
//===----------------------------------------------------------------------===//

static std::ifstream inputFile;
static std::string fileContent;
static std::istringstream inputStream;
int32_t main() {

  InitializeNativeTarget();
  InitializeNativeTargetAsmPrinter();
  InitializeNativeTargetAsmParser();
  auto JIT = TuringJIT::Create();
  if (!JIT) {
    llvm::errs() << "Failed to create JIT: " << toString(JIT.takeError())
                 << "\n";
    return 1;
  }

  initializeGlobals();
  fprintf(stderr, "ready> ");

  getNextToken();

  handleParseInput();
  handleParseBlank();
  handleParseStartState();
  handleParseTable();

  fprintf(stderr, "Parsing completed.\n");

  TheModule->print(outs(), nullptr);
  std::error_code EC;
  llvm::raw_fd_ostream OS("output.ll", EC, llvm::sys::fs::OF_None);
  TheModule->print(OS, nullptr);
  OS.flush();
  (*JIT)->addModule(std::move(TheModule));

  return 0;
}
